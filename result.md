# Smallest enclosing circle
- Center: 50-39 68th St, Queens, NY 11377
- Radius: about 11.8 miles
- Points on the circle:
  * Coney Island - Stilwell Av
  * Wakefield - 241 St
  * Far Rockway - Mott Av

See it [here](https://teensy.info/SL2uTmh38X)

(That redirects to https://www.mapdevelopers.com/draw-circle-tool.php?circles=%5B%5B18990.21%2C40.7357501%2C-73.8966468%2C%22%235E1BAA%22%2C%22%23FF1940%22%2C0.4%5D%5D fwiw)

# Most-separated
* Coney Island - Stilwell Av
* Wakefield - 241 St

23.6 miles as the crow flies

# Center of gravity
* Middle of Newtown Creek, near Penny Bridge Park
  ([40.729473, -73.936940](https://www.google.com/maps/place/40%C2%B043'46.1%22N+73%C2%B056'13.0%22W))
