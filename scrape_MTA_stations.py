from __future__ import annotations

import heapq
import io
import itertools
import math
import urllib
from typing import Any

import miniball                 # type: ignore [import-not-found]
import numpy as np
import pandas as pd
import requests
import tqdm
import utm                      # type: ignore [import-not-found]
from bs4 import BeautifulSoup
from joblib import Memory       # type: ignore [import-not-found]

url_prefix = "https://en.wikipedia.org"
memory = Memory("cachedir", verbose=0)


def MTA_station_article_titles() -> list[str]:
    wikiurl = f"{url_prefix}/wiki/List_of_New_York_City_Subway_stations#List_of_stations"
    response = requests.get(wikiurl)
    response.raise_for_status()

    soup = BeautifulSoup(response.text, "html.parser")
    tables = soup.select("table.wikitable.sortable")
    result = []
    for t in tables:
        parsed = pd.read_html(io.StringIO(str(t)), extract_links="all")[0]

        if [t[0] for t in parsed.columns[0:3]] == ["Station", "Services", "Div"]:
            station_name_column = parsed[("Station", None)]
            station_urls = [t[1] for t in station_name_column]

            for u in station_urls:
                unquoted = urllib.parse.unquote(u)
                unprefixed = unquoted.removeprefix("/wiki/")
                result.append(unprefixed.replace("_", " "))
            break
    return result


class WTFError(Exception):
    pass


@memory.cache  # type: ignore
def wikipedia_coordinates(title: str):
    URL = "https://en.wikipedia.org/w/api.php"

    PARAMS = {
        "action": "query",
        "format": "json",
        "titles": title,
        "prop": "coordinates",
    }

    response = requests.get(url=URL, params=PARAMS)
    response.raise_for_status()

    pages = response.json()["query"]["pages"]

    result = []
    for v in pages.values():
        if "coordinates" in v:
            result.append((v["coordinates"][0]["lat"], v["coordinates"][0]["lon"]))
        else:
            raise WTFError(response.text)

    return result


if __name__ == "__main__":
    utm_coordinates = []

    for title in tqdm.tqdm(MTA_station_article_titles(), total=470):
        try:
            for coordinate in wikipedia_coordinates(title):
                (easting, northing, zone_number, zone_letter) = utm.from_latlon(
                    *coordinate,
                )
                utm_coordinates.append([easting, northing])
        except WTFError as e:
            print(f"wtf? {e}")

    utm_coordinates_array = np.array(utm_coordinates)

    center, r_squared = miniball.get_bounding_ball(utm_coordinates_array)
    center_lat, center_lon = utm.to_latlon(
        center[0],
        center[1],
        zone_number,
        zone_letter,
    )
    print(
        f"Smallest enclosing circle: {center_lat=} {center_lon=}; {math.sqrt(r_squared)=} (in meters, I think?)",
    )

    print(
        "Center of gravity: ",
        utm.to_latlon(
            *np.mean(utm_coordinates_array, axis=0),
            zone_number,
            zone_letter,
        ),
    )

    point_pairs = itertools.combinations(utm_coordinates_array, r=2)

    def distance_between(point_pair: tuple[float, float]) -> np.floating[Any]:
        a, b = point_pair
        return np.linalg.norm(a - b)

    most_separateds = heapq.nlargest(3, point_pairs, key=distance_between) # type: ignore [arg-type]

    def to_lat_lon(utm_point: tuple[float, float]) -> Any:
        return utm.to_latlon(utm_point[0], utm_point[1], zone_number, zone_letter)

    for ms in most_separateds:
        print(
            "a very-separated pair: ",
            to_lat_lon(ms[0]),
            to_lat_lon(ms[1]),
        )
