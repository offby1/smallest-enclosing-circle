# WTF
I've occasionally wondered: what's the smallest enclosing circle that includes all of the NYC subway stations?

This wasn't too hard to figure out.  Here's how I did it:

* Learned about the Wikipedia API
* wrote the code you see here: it uses the Wikipedia API to fetch all the station coordinates from a page like [this](https://en.wikipedia.org/wiki/List_of_Long_Island_Rail_Road_stations#List_of_stations)
* used [this algorithm](https://en.wikipedia.org/wiki/Smallest-circle_problem#Welzl's_algorithm) to calculate the circle

tl;dr: as of 2023-10-01T20:03:07-0700 anyway, the center is 50-39 68th St, Queens, NY 11377, the radius is about 11.8 miles, and the two furthest-apart stations are Wakefield 241 St and Coney Island-Stillwell Avenue.

See [result](result.md) for a few more details.

# How to run it

- `uv run python scrape_MTA_stations.py`

That last step will take a minute the first time you run it, but subsequent runs will be fast since it caches the data that it downloads.

What? You don't know what "uv" is?  tsk tsk tsk.  <https://docs.astral.sh/uv/>

# TODO

- Figure out what to do if the locations don't just all happen to fall inside the same UTM grid.
